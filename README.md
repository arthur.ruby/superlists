# superlists

This project is a very basic to-do lists web app from the book "Obey The Testing Goat" (http://www.obeythetestinggoat.com/). The goal here is to learn Test-Driven Development (or TDD) and be able to create awesome robust projects in the future.